import { SensacionalFrontPage } from './app.po';

describe('sensacional-front App', () => {
  let page: SensacionalFrontPage;

  beforeEach(() => {
    page = new SensacionalFrontPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
