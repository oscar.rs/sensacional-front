import { ConditionService } from './services/condition.service';
import { BrandService } from './services/brand.service';
import { SectionService } from './services/section.service';
import { ProductService } from './services/product.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public products;
  public showTable;
  // Product select from list products
  public currentProduct: Object = {image: ''};

  constructor() {

  }


}
