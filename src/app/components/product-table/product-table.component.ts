import { ProductService } from './../../services/product.service';
import { AppConstants } from './../../app.constants';
import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-product-table',
  templateUrl: './product-table.component.html',
  styleUrls: ['./product-table.component.css']
})
export class ProductTableComponent implements OnChanges {

  // Products to print in table
  @Input('products') products: any;
  // Flag to know if the table is from homepage or from filter
  @Input('keys') keys: boolean;
  // Array to order products depends about the filter
  public productsOrder = [];
  // Filter supported for this component
  public filter = {seller: '', range: ''};
  // Product select from list products
  @Output() productSelect: EventEmitter<Object> = new EventEmitter();

  constructor(private _productService: ProductService) {}


  ngOnChanges() {
    this.filter.seller = '';
    this.productsOrder = [];
    if (this.products) {
      if (this.keys) {
        Object.keys(this.products).reverse().forEach(key => {
          this.productsOrder = this.productsOrder.concat(this.products[key].reverse());
        });
      }else {
        this.productsOrder = this.productsOrder.concat(this.products.reverse());
      }
    }
  }

  public filterSeller(): void {
    this.filter.seller = this.genericFilter(this.filter.seller, 'Yes', 'No');

    const productFilter = this.productsOrder.filter(product => {
      return product.stock_availability === this.filter.seller;
    });

    const productNoFilter =  this.productsOrder.filter(product => {
      return product.stock_availability !== this.filter.seller;
    });

    this.productsOrder = productFilter.concat(productNoFilter);
  }

  public filterPrice(): void {
    this.filter.range = this.genericFilter(this.filter.range, 'max', 'min');

    if (this.filter.range === 'max') {
      this.productsOrder = this.productsOrder.sort( (prod, product) => {
        return product.price - prod.price;
      });
    }else {
      this.productsOrder = this.productsOrder.sort( (prod, product) => {
        return  prod.price - product.price;
      });
    }
  }

  public showProduct(product): void {
    let url = AppConstants.IMAGE_PRODUCT + '&q=' + this.setWhiteSpaces(product.name);
    this._productService.getImage(url).subscribe(data => {
      product.image = data.items[0].image.thumbnailLink;
      this.productSelect.emit(product);
      document.getElementById('productPictureModal').click();
    });
  }

  private genericFilter(property, valueA, valueB): string {
    if (property) {
      property  = property === valueA ? valueB : valueA;
    }else {
      property = valueA;
    }
    return property;
  }

  // Replace white spaces in string to valid format url
  private setWhiteSpaces(value: string): string {
    while (value.includes(' ')) {
      value = value.replace(' ', '%20');
    }
    return value;
  }



}
