import { AppConstants } from './../../app.constants';
import { ProductService } from './../../services/product.service';
import { SectionService } from './../../services/section.service';
import { BrandService } from './../../services/brand.service';
import { ConditionService } from './../../services/condition.service';
import { Component, EventEmitter, Output } from '@angular/core';
@Component({
  selector: 'app-filter-var',
  templateUrl: './filter-var.component.html',
  styleUrls: ['./filter-var.component.css']
})
export class FilterVarComponent {


  @Output() newProducts: EventEmitter<Object> = new EventEmitter();
  @Output() showTable: EventEmitter<boolean> = new EventEmitter();
    // Product select from list products
  @Output() productSelect: EventEmitter<Object> = new EventEmitter();
  // All products with filter
  public dataTotal: Object[];
  // Products to get from modal
  public productsModal = [];
  // Show chart and box with data
  public showData = false;
  // Filter data from html template
  public filter: Object = {
    brand : '',
    categories: '',
    condition: '',
    name: ''
  };
  public brands: String[];
  public sections: String[];
  public conditions: String[];
  public modalTitle: string = '';
  public optionsChart = {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Sensacional Chart'
    },
    colors: ['#4fd82f', '#db1b1b'],
    xAxis: {
        categories: []
    },
    series: [{
        name: 'Vendidos',
        data: []
    },
    {
        name: 'No vendidos',
        data: []
    }]
  };


  constructor(private _sectionService: SectionService,
              private _brandService: BrandService,
              private _productService: ProductService,
              private _conditionService: ConditionService) {
                this._brandService.get().subscribe(brands => this.brands = brands.brands);
                this._sectionService.get().subscribe(sections => this.sections = sections.sections);
                this._conditionService.get().subscribe(conditions => this.conditions = conditions.conditions);
              }

  // Get information when the user click to Bar Chart and open filter modal
  onPointSelect (e): void {
    let keySelect = e.category.split(',')[0].replace(' $ ', '');
    this.modalTitle = keySelect + '$ ' + e.series.name;
    let modalArray = this.dataTotal.find( (obj) => {
      return Object.keys(obj).includes(keySelect);
    });
    e.series.name === 'Vendidos' ? this.productsModal = modalArray['sellers'] : this.productsModal = modalArray['nosellers'];
    document.getElementById('openModalButton').click();
  }

  public search(): void {
    this.showData = false;
    this.showTable.emit(this.showData);
    this.requestFilter(this.setQuery());
  }


  // Load data chart from Backend.
  private requestFilter(url: string): void {
    this.dataTotal = [];
    this.optionsChart.xAxis.categories = [];
    this.optionsChart.series[0].data = [];
    this.optionsChart.series[1].data = [];
    this._productService.filter(url).subscribe( (res) => {
      this.newProducts.emit(res.products);
      Object.keys(res.products).forEach((key) => {
        this.dataTotal.push({ sellers: res.products[key].filter((product) => {
                      return product.qty === '1';
                    }),
                    nosellers: res.products[key].filter((product) => {
                      return product.qty === '0';
                    }),
                    products: res.products[key]
                  });
                  this.optionsChart.xAxis.categories.push(key.replace('Rango', '') + ' $ , TOTAL = ' + res.products[key].length);
                  this.optionsChart.series[0].data.push(this.dataTotal[this.dataTotal.length - 1]['sellers'].length);
                  this.optionsChart.series[1].data.push(this.dataTotal[this.dataTotal.length - 1]['nosellers'].length);
                  this.dataTotal[this.dataTotal.length - 1][key] = res.products[key].length;
      });
                this.showData = true;
                this.showTable.emit(this.showData);
    });
  }


  public selectFilter(product): void {
    this.productSelect.emit(product);
  }


  // set queries string format to do a request
  private setQuery(): string {
    let url = AppConstants.BASE_URL + '/filter?';
    let currentIndex = 0;
    Object.keys(this.filter).forEach((key) => {
      if (this.filter[key]) {
        currentIndex++;
        url += key + '=' + this.setWhiteSpaces(this.filter[key]);
        const totalQueries = Object.keys(this.filter).filter( key => {
          return !!this.filter[key];
        });
        if (currentIndex !== totalQueries.length) {
          url += '&';
        }
      }
    });
    return url;
  }

  // Replace white spaces in string to valid format url
  private setWhiteSpaces(value: string): string {
    while (value.includes(' ')) {
      value = value.replace(' ', '%20');
    }
    return value;
  }

}
