import { ProductTableComponent } from './components/product-table/product-table.component';
import { FilterVarComponent } from './components/filter-var/filter-var.component';
import { ConditionService } from './services/condition.service';
import { BrandService } from './services/brand.service';
import { SectionService } from './services/section.service';
import { ProductService } from './services/product.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ChartModule } from 'angular2-highcharts';
import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService';
export function highchartsFactory() {
  return require('highcharts');
}


import { AppComponent } from './app.component';
@NgModule({
  declarations: [
    AppComponent,
    FilterVarComponent,
    ProductTableComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ChartModule
  ],
  providers: [
    {
      provide: HighchartsStatic,
      useFactory: highchartsFactory
    },
    ProductService,
    SectionService,
    BrandService,
    ConditionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
