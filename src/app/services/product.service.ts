import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class ProductService {

  constructor(private http: Http) { }


    public get(): Observable<any> {
        return this.http.get('http://localhost:3000/data')
        .map((response: Response) => response.json());
    }

    public filter(url: string): Observable<any> {
        return this.http.get(url)
        .map((response: Response) => response.json());
    }

    public getImage(url): Observable<any> {
        return this.http.get(url)
        .map((response: Response) => response.json());
    }

}
