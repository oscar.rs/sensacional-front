import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class ConditionService {

  constructor(private http: Http) { }

  public get(): Observable<any> {
    return this.http.get('http://localhost:3000/conditions')
            .map((response: Response) => response.json());
  }

}
