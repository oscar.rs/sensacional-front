import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';


@Injectable()
export class SectionService {

  constructor(private http: Http) {}

    public get(): Observable<any> {
      return this.http.get('http://localhost:3000/sections')
              .map((response: Response) => response.json());
    }

}


